# Atlassian Annotations

## Description

Standard annotation types for declaring API status and properties.

## Atlassian Developer?

### Committing Guidelines

Please see [The Cloud Platform Rules of Engagement (go/cproe)](http://go.atlassian.com/cproe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/AA)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/AA)

### Documentation

[Javadoc](https://docs.atlassian.com/atlassian-annotations/)
