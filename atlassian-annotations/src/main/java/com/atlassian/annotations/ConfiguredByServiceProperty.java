package com.atlassian.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark a method whose implementation can be configured by properties from the service environment.
 *
 * @since v1.2.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface ConfiguredByServiceProperty {
    /**
     * @return a string identifier for grouping the configuration into a higher level feature.
     */
    String group();

    /**
     * The name of the configuration which alters the implementation behaviour. This should be the name of a system
     * property, or a server specific name for a dark feature or other configuration mechanism.
     */
    String property();
}
