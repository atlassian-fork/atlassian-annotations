package com.atlassian.annotations.nonnull;

import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Similar to {@link javax.annotation.ParametersAreNonnullByDefault}, but applies to fields of classes.
 *
 * @deprecated since 2.2.0. Use corresponding Checker Framework based annotation {@link com.atlassian.annotations.nullability.FieldsAreNonnullByDefault}
 */
@Documented
@Nonnull
@TypeQualifierDefault({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface FieldsAreNonnullByDefault {
}
