/**
 * This package provides nonnull and nullable annotations for fields and return values, similar to
 * {@link javax.annotation.ParametersAreNonnullByDefault} and {@link javax.annotation.ParametersAreNullableByDefault}.
 * For some reason, JSR-305 only has these annotations for parameters, but allows you to build your own versions for
 * fields and return values.
 * <p>
 *     Both classes and packages ({@code package-info.java}) can be annotated.
 *     If a package is annotated then subpackages <b>are not</b> also included. Subpackages must be
 *     annotated individually.
 * </p>
 */
package com.atlassian.annotations.nonnull;
