package com.atlassian.annotations.nonnull;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.annotation.Nullable;
import javax.annotation.meta.TypeQualifierDefault;

/**
 * Similar to {@link javax.annotation.ParametersAreNullableByDefault}, but applies to fields of classes.
 *
 * @deprecated since 2.2.0. Use corresponding Checker Framework based annotation {@link com.atlassian.annotations.nullability.FieldsAreNullableByDefault}
 */
@Documented
@Nullable
@TypeQualifierDefault({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface FieldsAreNullableByDefault {
}
