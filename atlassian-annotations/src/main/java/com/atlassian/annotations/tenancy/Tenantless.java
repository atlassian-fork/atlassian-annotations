package com.atlassian.annotations.tenancy;


import com.atlassian.annotations.Internal;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the component only accesses data that is universal across all tenants.
 * <p>
 * Tenantless components may be accessed safely even outside of a tenanted request context and must avoid
 * access to any tenant-specific information.
 * </p>
 *
 */
@Internal
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Deprecated
public @interface Tenantless {
    /**
     * Explanatory message/reason why given component was marked as @Tenantless.
     */
    String reason() default "";
}
