package com.atlassian.api.analyser.rules;

import com.atlassian.api.analyser.ApiClass;

import java.util.List;
import java.util.Map;

/**
 * You are not allowed to remove an api class.
 *
 * @since v5.0
 */
public class NoRemoveClassRule implements Rule {

    public void apply(String className, Map<String, ApiClass> baseline, Map<String, ApiClass> current, List<String> errors) {
        // The class in the baseline should also present in the current;
        ApiClass currentClass = current.get(className);
        ApiClass baselineClass = baseline.get(className);
        if (baselineClass != null) {
            if (currentClass == null) {
                errors.add("Class - '" + className + "' has been removed.");
            }
        }
    }
}
