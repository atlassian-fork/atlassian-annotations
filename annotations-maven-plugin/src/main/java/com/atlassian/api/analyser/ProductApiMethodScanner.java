package com.atlassian.api.analyser;

import org.dom4j.Element;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 */
public class ProductApiMethodScanner extends MethodVisitor {
    private Element methodElement;

    public ProductApiMethodScanner(final Element methodElement) {
        super(Opcodes.ASM5);
        this.methodElement = methodElement;
    }

    public AnnotationVisitor visitAnnotation(final String desc, final boolean visible) {
        // We never expect these 2 but we will leave them here for now.
        if (desc.equals("Lcom/atlassian/annotations/PublicApi;")) methodElement.addAttribute("api", "true");
        if (desc.equals("Lcom/atlassian/annotations/PublicSpi;")) methodElement.addAttribute("spi", "true");

        // It is reasonable to mark a method as internal or experimental in an api or spi class.
        if (desc.equals("Lcom/atlassian/annotations/Internal;")) methodElement.addAttribute("internal", "true");
        if (desc.equals("Lcom/atlassian/annotations/ExperimentalApi;"))
            methodElement.addAttribute("experimental", "true");
        return null;
    }


    public AnnotationVisitor visitAnnotationDefault() {
        return null;
    }


    public AnnotationVisitor visitParameterAnnotation(int parameter, String desc, boolean visible) {
        return null;
    }


    public void visitAttribute(Attribute attr) {
    }


    public void visitCode() {
    }


    public void visitFrame(int type, int nLocal, Object[] local, int nStack, Object[] stack) {
    }


    public void visitInsn(int opcode) {
    }


    public void visitIntInsn(int opcode, int operand) {
    }


    public void visitVarInsn(int opcode, int var) {
    }


    public void visitTypeInsn(int opcode, String type) {
    }


    public void visitFieldInsn(int opcode, String owner, String name, String desc) {
    }


    public void visitMethodInsn(int opcode, String owner, String name, String desc) {
    }


    public void visitJumpInsn(int opcode, Label label) {
    }


    public void visitLabel(Label label) {
    }


    public void visitLdcInsn(Object cst) {
    }


    public void visitIincInsn(int var, int increment) {
    }


    public void visitTableSwitchInsn(int min, int max, Label dflt, Label[] labels) {
    }


    public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
    }


    public void visitMultiANewArrayInsn(String desc, int dims) {
    }


    public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
    }


    public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
    }


    public void visitLineNumber(int line, Label start) {
    }


    public void visitMaxs(int maxStack, int maxLocals) {
    }


    public void visitEnd() {
    }
}
