package com.atlassian.api.analyser.rules;

import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiMethod;

import java.util.List;
import java.util.Map;

/**
 * You are not allowed to remove abstract methods from an spi class or interface.
 *
 * @since v5.0
 */
public class NoRemoveAbstractMethodRule implements Rule {

    public void apply(String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        // For the class in the baseline ensure all public methods are also present in the current;
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiMethod apiMethod : baseLineClass.getMethods().keySet()) {
            if (apiMethod.isInternal() || apiMethod.isExperimental()) {
                continue;
            }
            if (apiMethod.isAbstractMethod()) {
                if (!currentClass.getMethods().containsKey(apiMethod)) {
                    errors.add("Class - '" + className + "' : Abstract method '" + apiMethod.getMethodSignature() + "' removed from class.");
                }
            }
        }
    }
}
