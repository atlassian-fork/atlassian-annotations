package com.atlassian.api.analyser.rules;

import com.atlassian.api.analyser.Access;
import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiMethod;

import java.util.List;
import java.util.Map;

/**
 * You are not allowed to remove public methods from an api class.
 *
 * @since v5.0
 */
public class NoRemovePublicMethodRule implements Rule {

    public void apply(String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        // For the class in the baseline ensure all public methods are also present in the current;
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiMethod baselineMethod : baseLineClass.getMethods().keySet()) {
            if (baselineMethod.getAccess() == Access.PUBLIC) {
                if (baselineMethod.isInternal() || baselineMethod.isExperimental()) {
                    continue;
                }
                ApiMethod currentMethod = currentClass.getMethods().get(baselineMethod);
                if (currentMethod == null) {
                    errors.add("Class - '" + className + "' : Public method '" + baselineMethod.getMethodSignature() + "' removed from class.");
                } else if (currentMethod.getAccess() != Access.PUBLIC) {
                    errors.add("Class - '" + className + "' : Accessibility of public method '" + baselineMethod.getMethodSignature() + "' has been weakened.");
                }

            }
        }
    }
}
