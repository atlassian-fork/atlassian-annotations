package com.atlassian.api.analyser.rules;

import com.atlassian.api.analyser.Access;
import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiField;

import java.util.List;
import java.util.Map;

/**
 * You are not allowed to remove protected or public fields from an spi abstract class.
 *
 * @since v5.0
 */
public class NoRemoveProtectedFieldRule implements Rule {

    public void apply(String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        // For the class in the baseline ensure all public fields are also present in the current;
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiField apiField : baseLineClass.getFields().keySet()) {
            if (apiField.getAccess() == Access.PUBLIC || apiField.getAccess() == Access.PROTECTED) {
                if (!currentClass.getFields().containsKey(apiField)) {
                    errors.add("Class - '" + className + "' : '" + apiField.getAccess() + "' field '" + apiField.getFieldName() + "' removed from class.");
                }
            }
        }
    }
}
