package com.atlassian.annotations;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @goal package
 */
public class PackageMojo extends AbstractAnnotationsMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        final PublicApiAnalyser analyser = getPublicApiAnalyser();

        for (final AnalysisResult result : analyser.analyse()) {

        }
    }
}
