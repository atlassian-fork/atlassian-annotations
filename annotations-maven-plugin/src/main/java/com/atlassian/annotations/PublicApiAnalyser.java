package com.atlassian.annotations;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.scannotation.AnnotationDB;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

public class PublicApiAnalyser {
    private final ClassLoader classLoader;
    private final Set<String> apiSet;
    private final AnnotationDB db = new AnnotationDB();
    private final Set<String> excludedPackages = ImmutableSet.<String>builder().add("javax", "java").build();

    public PublicApiAnalyser(Set<URL> urlsToScan, Set<URL> urlsForClasspath) throws IOException {
        db.scanArchives(urlsToScan.toArray(new URL[urlsToScan.size()]));

        final Set<String> publicApiClasses = db.getAnnotationIndex().get(PublicApi.class.getName());
        final Set<String> experimentalApiClasses = db.getAnnotationIndex().get(ExperimentalApi.class.getName());

        apiSet = Sets.union(publicApiClasses, experimentalApiClasses).copyInto(new HashSet<String>());
        classLoader = new URLClassLoader(urlsForClasspath.toArray(new URL[urlsForClasspath.size()]), null);
    }

    public Set<AnalysisResult> analyse() {
        final Set<AnalysisResult> analysisResults = new HashSet<AnalysisResult>();

        for (String annotatedClassName : apiSet) {
            AnalysisResult analysisResult = new AnalysisResult(annotatedClassName);
            Class<?> clazz = null;
            try {
                clazz = classLoader.loadClass(analysisResult.getClassName());
            } catch (ClassNotFoundException e) {
                analysisResult.setUnloadable();
            }

            if (!analysisResult.isUnloadable()) {
                analyzeMethods(analysisResult, clazz);
            }

            analysisResults.add(analysisResult);
        }

        return analysisResults;
    }

    private void analyzeMethods(AnalysisResult analysisResult, Class<?> clazz) {
        for (Method method : clazz.getDeclaredMethods()) {
            //Check all params
            for (Class<?> param : method.getParameterTypes()) {
                if (isNotAnnotated(param)) {
                    analysisResult.addUnannotatedMethodReference(method, param);
                }
            }

            //Check all methods
            final Class<?> returnType = method.getReturnType();
            if (isNotAnnotated(returnType)) {
                analysisResult.addUnannotatedMethodReference(method, returnType);
            }
        }
    }

    private boolean isNotAnnotated(final Class<?> param) {
        return !isPublicApi(param) && param.getPackage() != null && !isExcluded(param.getPackage().getName());
    }

    private boolean isPublicApi(Class<?> aClass) {
        return apiSet.contains(aClass.getName());
    }

    private boolean isExcluded(String name) {
        for (String packageName : excludedPackages) {
            if (name.startsWith(packageName)) {
                return true;
            }
        }
        return false;
    }
}
