package com.atlassian.annotations;


import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

/**
 * @goal verify
 */
public class VerifyMojo extends AbstractAnnotationsMojo {
    /**
     * @parameter expression="${annotation.fail}" default-value="true"
     */
    boolean failOnError;

    public void execute() throws MojoExecutionException, MojoFailureException {
        final PublicApiAnalyser publicApiAnalyser = getPublicApiAnalyser();

        boolean shouldFail = false;
        for (AnalysisResult analysisResult : publicApiAnalyser.analyse()) {
            Map<Method, Set<Class<?>>> refs = analysisResult.getNonAnnotatedMethodReferences();
            if (!refs.isEmpty()) {
                shouldFail = true;
                for (Map.Entry<Method, Set<Class<?>>> methodClassEntry : refs.entrySet()) {
                    final Method method = methodClassEntry.getKey();
                    final String methodName = method.getName();
                    getLog().info(analysisResult.getClassName() + "#" + methodName + "");
                    getLog().info("\tNon-annotated types:");
                    for (Class<?> aClass : methodClassEntry.getValue()) {
                        getLog().info("\t\t" + aClass.getName());
                    }
                }
            }
        }

        if (failOnError && shouldFail) {
            throw new MojoFailureException("APIs were not correctly annotated with @PublicApi or @ExperimentalApi");
        }
    }
}
