package com.atlassian.annotations;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.InvalidProjectModelException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AbstractAnnotationsMojo extends AbstractMojo {
    private static final Function<Artifact, URL> ARTIFACT_TO_URL_FUNCTION = new Function<Artifact, URL>() {
        public URL apply(final Artifact artifact) {
            try {
                return artifact.getFile().toURI().toURL();
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }
    };

    private static final Predicate<Artifact> ARTIFACT_MUST_EXIST_PREDICATE = new Predicate<Artifact>() {
        public boolean apply(final Artifact input) {
            return input.getFile() != null && input.getFile().exists();
        }
    };

    private static final Function<MavenProject, Artifact> PROJECT_TO_ARTIFACT_FUNCTION = new Function<MavenProject, Artifact>() {
        public Artifact apply(final MavenProject input) {
            return input.getArtifact();
        }
    };

    /**
     * @parameter expression="${project}"
     * @required
     */
    protected MavenProject project;

    /**
     * @component
     */
    protected ArtifactFactory artifactFactory;

    /**
     * All projects listed in the reactor
     *
     * @parameter expression="${reactorProjects}"
     * @required
     */
    protected List<MavenProject> reactorProjects;

    /**
     * @component
     */
    protected MavenProjectBuilder projectBuilder;

    /**
     * @component
     */
    protected ArtifactResolver resolver;

    /**
     * @parameter default-value="${localRepository}"
     * @required
     * @readonly
     */
    protected ArtifactRepository localRepository;

    /**
     * @parameter default-value="${project.remoteArtifactRepositories}"
     * @required
     * @readonly
     */
    protected List remoteRepositories;

    /**
     * @component
     */
    protected ArtifactMetadataSource metadataSource;

    protected PublicApiAnalyser getPublicApiAnalyser() throws MojoExecutionException {
        final Set<URL> urlsToReactorProjects = getArtifactsFromReactorAsURLs();

        final Set<URL> classPathUrls = getAllDependenciesAsURLs();

        PublicApiAnalyser publicApiAnalyser;
        try {
            publicApiAnalyser = new PublicApiAnalyser(urlsToReactorProjects, classPathUrls);
        } catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        return publicApiAnalyser;
    }

    private Set<URL> getAllDependenciesAsURLs() throws MojoExecutionException {
        return Sets.newHashSet(Collections2.transform(getAllDependencies(), ARTIFACT_TO_URL_FUNCTION));
    }

    private Set<URL> getArtifactsFromReactorAsURLs() {
        return Sets.newHashSet(Collections2.transform(getArtifactsFromReactor(), ARTIFACT_TO_URL_FUNCTION));
    }

    private Set<Artifact> getAllDependencies() throws MojoExecutionException {
        final Set<Artifact> projects = getReactorProjectDependencies();

        if (projects == null) {
            throw new MojoExecutionException("Could not resolve dependent projects");
        }

        if (reactorProjects == null || reactorProjects.size() < 1) {
            throw new MojoExecutionException("There are no dependent projects left to process.");
        }

        return projects;
    }

    protected Set<Artifact> getArtifactsFromReactor() {
        return Sets.newHashSet(Collections2.filter(Collections2.transform(reactorProjects, PROJECT_TO_ARTIFACT_FUNCTION), ARTIFACT_MUST_EXIST_PREDICATE));
    }

    private Collection<Artifact> filterByFileExisting(Set<Artifact> artifacts) {
        return Collections2.filter(artifacts, new Predicate<Artifact>() {
            public boolean apply(final Artifact input) {
                return input.getFile() != null && input.getFile().exists();
            }
        });
    }

    private Set<Artifact> getReactorProjectDependencies() throws MojoExecutionException {
        Set<Artifact> dependencies = new HashSet();
        for (MavenProject mavenProject : reactorProjects) {
            Collection<Artifact> projectDependencies = Collections2.filter(mavenProject.getDependencyArtifacts(), new Predicate<Artifact>() {
                public boolean apply(final Artifact input) {
                    return input.getScope().equals("compile");
                }
            });
            if (projectDependencies != null && !projectDependencies.isEmpty()) {
                dependencies.addAll(projectDependencies);
            }
        }
        return Sets.newHashSet(Collections2.filter(resolveTransitiveArtifacts(dependencies), ARTIFACT_MUST_EXIST_PREDICATE));
    }

    /*
     * Resolves transitive dependencies for a set of given artifacts
     * Returns a Set of all artifacts resolved
     */
    private Set<Artifact> resolveTransitiveArtifacts(Set<Artifact> artifacts, ArtifactFilter filter)
            throws MojoExecutionException {
        try {
            ArtifactResolutionResult result = resolver.resolveTransitively(artifacts, project.getArtifact(),
                    localRepository, remoteRepositories, metadataSource, filter);
            return result.getArtifacts();
        } catch (ArtifactResolutionException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        } catch (ArtifactNotFoundException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private Set<Artifact> resolveTransitiveArtifacts(Set<Artifact> artifacts) throws MojoExecutionException {
        return resolveTransitiveArtifacts(artifacts, null);
    }


    /*
     * Returns the MavenProject for a given Artifact
     */
    private MavenProject getProjectForArtifact(Artifact artifact)
            throws MojoExecutionException {
        try {
            return projectBuilder.buildFromRepository(artifact, remoteRepositories, localRepository);
        } catch (InvalidProjectModelException e) {
            getLog().error("Validation Errors: " + e.getValidationResult().getMessages());
            throw new MojoExecutionException(e.getMessage(), e);
        } catch (ProjectBuildingException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

}